<?php
/**
 * Created by PhpStorm.
 * User: tiago
 * Date: 20/01/18
 * Time: 14:02
 */

namespace Tests\Controller;
use App\Country;
use App\Http\Controllers\CountryController;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;


class CountryTest extends TestCase
{
    public function testIndex()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    public function testGetCountry()
    {
        $response = $this->get('/getCountry');
        $response->assertStatus(200);
    }

    public function testDescription()
    {
        $country = new CountryController();
        $this->assertEquals('(BR) BRAZIL',$country->description('BRAZIL'));
    }
}