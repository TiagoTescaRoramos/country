Sistema feito em Laravel 5.5 no back-end e Semantic-ui-react no front-end.
Tem 3 menus, aonde o primeiro mostra uma tabela e os outros dois baixa um arquivo excel.
Tem alguns testes unitários feitos. Precisa instalar as dependências com o composer e
caso queira mudar o front-end, basta executar npm install.