<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert('INSERT INTO country(name,date) VALUE("Brazil",NOW());');
        DB::insert('INSERT INTO country(name,date) VALUE("USA",NOW());');
        DB::insert('INSERT INTO country(name,date) VALUE("England",NOW());');
        DB::insert('INSERT INTO country(name,date) VALUE("Germany",NOW());');
    }
}
