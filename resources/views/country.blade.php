<!doctype html>
<html>
    <head>
        <title>Coutry</title>
        <link rel="stylesheet" href="<?echo url()->full();?>/css/semantic.min.css">
        <script src="<?echo url()->full();?>/js/jquery-3.3.1.min.js"></script>
    </head>
    <body>
        <div id="menu"></div>
        <div id="table"></div>
    </body>
    <footer>
        <script src="<?echo url()->full();?>/js/app.js"></script>
    </footer>
</html>