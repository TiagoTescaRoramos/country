import React from 'react'
import { Menu } from 'semantic-ui-react'
import ReactDOM from "react-dom";
import TableDados from './table'

const list = function () {
    jQuery.ajax({
        url: '/getCountry',
        dataType: 'JSON',
        success: function (res) {
            ReactDOM.render(
                <TableDados data={res}/>,
                document.getElementById('table')
            );
        }
    })
};

const csv = function () {
    document.location.href = "/getCountryCSV";
};

const xls = function () {
    document.location.href = "/getCountryXLS";
}
const items = [
    { key: 'list',name: 'List Country', onClick: list},
    { key: 'csv', name: 'Download CSV', onClick: csv},
    { key: 'xls', name: 'Download XLS', onClick: xls}
]

const MenuExampleProps = () => (
    <Menu items={items} />
)

export default MenuExampleProps