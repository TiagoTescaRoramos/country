import React, {Component} from 'react'
import { Icon, Label, Menu, Table } from 'semantic-ui-react'

class TableDados extends Component{
    render() {
        return (
            <Table celled>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>ID</Table.HeaderCell>
                        <Table.HeaderCell>NAME</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body>{
                    this.props.data.map((obj, id) =>
                        <Table.Row>
                            <Table.Cell>{obj['id']}</Table.Cell>
                            <Table.Cell>{obj['name']}</Table.Cell>
                        </Table.Row>)
                }
                </Table.Body>
            </Table>
        );
    }
}

export default TableDados