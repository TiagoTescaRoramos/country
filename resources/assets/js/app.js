'use strict';
import React from 'react'
import ReactDOM from 'react-dom'
import MenuExampleProps from './components/menu'

ReactDOM.render(
    <MenuExampleProps/>,
    document.getElementById('menu')
);