const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const path = require('path')
const analyze = !!process.env.ANALYZE_ENV


const webpackConfig = {
    name: 'client',
    target: 'web',
    entry: {
        app: path.resolve('resources/assets/js/app.js'),
    },
    module: {
        loaders: [{
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            loader: 'babel',
            query: {
                presets: ['es2015', 'react']
            }
        }],
    },
    output: {
        filename: '[name].js',
        path: path.resolve('public/js'),
        publicPath: '/',
    },
    resolveLoader: {
       moduleExtensions: ["-loader"]
    }
}

if (analyze) {
    webpackConfig.plugins.push(new BundleAnalyzerPlugin())
}
module.exports = webpackConfig