<?php
/**
 * Created by PhpStorm.
 * User: Tiago Rodrigo Tescaro Ramos
 * Email tiago.tescaro@hotmail.com
 * Date: 20/01/18
 * Time: 13:15
 */

namespace App\Http\Controllers;

use App\Country;
use Maatwebsite\Excel\Facades\Excel;

class CountryController extends Controller
{
    public function index()
    {
        return view('country',[]);
    }

    public function getCountry()
    {
        echo $country = Country::orderBy('name','desc')->get()->toJson();
    }

    public function description($name)
    {
        $simb = substr($name,0,2);
        $simb = strtoupper($simb);
        return "($simb) $name";
    }

    public function getCountryCSV()
    {
        $country = Country::orderBy('name','desc')->get();
        $path = '/var/www/html/Country/Temp/country.csv';
        $fp = fopen($path,'w');
        fwrite($fp,"ID;NAME;DESCRIPTION\n");
        foreach ($country as $value) {
            fwrite($fp,"{$value['id']};{$value['name']};{$this->description($value['name'])}\n");
        }
        fclose($fp);
        //Poderia utilizar file_put_contents tambem.
        return response()->download($path);
    }

    public function getCountryXLS()
    {
        Excel::create('Country', function ($excel){
            $excel->sheet('Country', function($sheet) {
                $country = Country::orderBy('name','desc')->get();
                $line = 1;
                $sheet->row($line++,['ID','NAME','DESCRIPTION']);
                foreach ($country as $value) {
                    $sheet->row($line++, [$value['id'], $value['name'], $this->description($value['name'])]);
                }
                $sheet->setAutoSize(true);
                $sheet->setFontSize(10);
            });
        })->export('xls');
    }
}
